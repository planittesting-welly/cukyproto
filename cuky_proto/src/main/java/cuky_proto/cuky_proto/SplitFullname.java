package cuky_proto.cuky_proto;

public class SplitFullname{

	// properties Devs can use
	public String firstname;
	public String lastname;
	
	private String name = null;
	
	// constructor
	public SplitFullname(String fullname){
		
		this.name = fullname;
				
	}
	
	public void split() throws Throwable{
		
		if (name != null){
		 String[] splitnames = name.split(" ");
		 firstname = splitnames[0];
		 lastname = splitnames[splitnames.length-1];
		}
		else throw new Exception("Full name was null");
		
	}
	
}
