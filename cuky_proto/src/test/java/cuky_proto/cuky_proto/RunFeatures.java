package cuky_proto.cuky_proto;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/concordian_ex.feature",
				glue = "steps",				
				plugin={"pretty", "html:out.html/first", "junit:out.html/first/cucumber.xml", "json:out.html/first/report.json"},
				dryRun = false)
public class RunFeatures {

	// Execute Maven --> Update Project after test passed to see output results
	
}
