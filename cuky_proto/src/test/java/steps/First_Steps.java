package steps;

import java.util.*;
import java.util.concurrent.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.support.ui.*;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;


public class First_Steps {

private WebDriver wd;
private List<WebElement> myLinks;

//@Before
public void Setup(){

	wd = new FirefoxDriver();
	wd.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
	
		
}

//@After
public void TearDown(){
	
	wd.quit();
	wd = null;
}


@Given("^the web \"([^\"]*)\"$")
public void the_web(String web_url) throws Throwable 
{
	Setup();
	wd.get(web_url);
	    // Write code here that turns the phrase above into concrete actions
	    
}

@When("^I click on the menu links$")
public void i_click_on_the_menu_links() throws Throwable {
	
	try 
	{	
		myLinks =  wd.findElements(By.tagName("a"));
		WebDriverWait wwait = new WebDriverWait(wd, 5);
		//String source = wd.getPageSource();
				
		for (int i=0; i<myLinks.size(); i++)
		{
			try{
				WebElement elem = myLinks.get(i);				
				if (!elem.getAttribute("class").contains("hidden") && elem.isDisplayed())
				{			
					wwait.until(ExpectedConditions.elementToBeClickable(elem));
					System.out.println(elem.getText());
					elem.click();					
					
					wd.navigate().back();
					Thread.sleep(1000);
					wwait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='form']/section/header/div[1]/div/div[2]/a[3]")));
					myLinks =  wd.findElements(By.tagName("a"));
					
					
				}
			}
			catch (Exception e){
				
				System.out.println(e.getMessage());
				//myLinks =  wd.findElements(By.tagName("a"));
			}
			
			
			
		}
	} catch (Exception e) {
		System.out.println("\n** Error:\n" + e.getMessage() + "**\n");
		throw new AssertionError("Assertion happened");
	}
	// Write code here that turns the phrase above into concrete actions
    
}


@Then("^All of them should work$")
public void all_of_them_should_work() throws Throwable {
    
	TearDown();
	// Write code here that turns the phrase above into concrete actions
    
}

}
