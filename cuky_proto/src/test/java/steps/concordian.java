package steps;

import cuky_proto.cuky_proto.*;

import cucumber.api.java.en.*;


import org.junit.*;

public class concordian {
	
    SplitFullname mySplit;

	@Given("^full name \"([^\"]*)\"$")
	public void full_name(String fullname) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	 mySplit = new SplitFullname(fullname);
	}

	@When("^importing the name$")
	public void importing_the_name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    mySplit.split();
	}

	@Then("^firstname is \"([^\"]*)\" and lastname is \"([^\"]*)\"$")
	public void firstname_is_and_lastname_is(String first, String last) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   Assert.assertEquals("First name",first, mySplit.firstname);
	   Assert.assertEquals("Last name", last, mySplit.lastname);
	}
	
	
}
